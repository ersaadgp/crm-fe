export const SHOW_MESSAGE = 'show_message';
export const HIDE_MESSAGE = 'hide_message';
export const FETCH_START = 'fetch_start';
export const FETCH_SUCCESS = 'fetch_success';
export const FETCH_ERROR = 'fetch_error';

export const UPDATE_AUTH_USER = 'update_auth_user';
export const UPDATE_LOAD_USER = 'update_load_user';
export const SEND_FORGET_PASSWORD_EMAIL = 'send_forget_password_email';
export const SIGNIN_GOOGLE_USER_SUCCESS = 'signin_google_user_success';
export const SIGNIN_FACEBOOK_USER_SUCCESS = 'signin_facebook_user_success';
export const SIGNIN_TWITTER_USER_SUCCESS = 'signin_twitter_user_success';
export const SIGNIN_GITHUB_USER_SUCCESS = 'signin_github_user_SUCCESS';
export const SIGNIN_USER_SUCCESS = 'signin_user_success';
export const SIGNOUT_USER_SUCCESS = 'signout_user_success';
export const PASSWORD_CHANGED = 'password_changed';

// Products
export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCT = 'GET_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CLEAR_PRODUCT = 'CLEAR_PRODUCT';
export const GET_PRODUCTS_COMBO = 'GET_PRODUCTS_COMBO';

// Sizes
export const GET_SIZES = 'GET_SIZES';
export const GET_SIZE = 'GET_SIZE';
export const UPDATE_SIZE = 'UPDATE_SIZE';
export const CREATE_SIZE = 'CREATE_SIZE';
export const DELETE_SIZE = 'DELETE_SIZE';
export const GET_SIZES_COMBO = 'GET_SIZES_COMBO';

// Tx
export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';

// Customers
export const GET_CUSTOMERS = 'GET_CUSTOMERS';
export const GET_CUSTOMER = 'GET_CUSTOMER';
export const UPDATE_CUSTOMER = 'UPDATE_CUSTOMER';
export const CREATE_CUSTOMER = 'CREATE_CUSTOMER';
export const DELETE_CUSTOMER = 'DELETE_CUSTOMER';
export const CLEAR_CUSTOMER = 'CLEAR_CUSTOMER';
export const GET_CUST_COMBOS = 'GET_CUST_COMBOS';

// Travel Packages
export const GET_PACKAGES = 'GET_PACKAGES';
export const GET_PACKAGE = 'GET_PACKAGE';
export const UPDATE_PACKAGE = 'UPDATE_PACKAGE';
export const CREATE_PACKAGE = 'CREATE_PACKAGE';
export const DELETE_PACKAGE = 'DELETE_PACKAGE';
export const CLEAR_PACKAGE = 'CLEAR_PACKAGE';
export const GET_PACK_COMBOS = 'GET_PACK_COMBOS';

// Orders
export const GET_ORDERS = 'GET_ORDERS';
export const GET_ORDER = 'GET_ORDER';
export const CREATE_ORDER = 'CREATE_ORDER';
export const CLEAR_ORDER = 'CLEAR_ORDER';
export const GET_ORDER_COMBOS = 'GET_ORDER_COMBOS';
export const CREATE_ORDER_DETAIL = 'CREATE_ORDER_DETAIL';
export const GET_ORDER_DETAIL = 'GET_ORDER_DETAIL';
