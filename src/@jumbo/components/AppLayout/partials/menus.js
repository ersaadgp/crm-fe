import IntlMessages from '../../../utils/IntlMessages';
import { People, CardTravel, Receipt } from '@material-ui/icons';
import React from 'react';

const role = localStorage.getItem('role');
const isAdmin = role === 'admin';

const dashboard = [
  {
    name: 'Customers',
    icon: <People />,
    type: 'item',
    link: '/dashboard/customers',
  },
  {
    name: 'Travel Package',
    icon: <CardTravel />,
    type: 'item',
    link: '/dashboard/travel-packages',
  },
  {
    name: 'Orders',
    icon: <Receipt />,
    type: 'item',
    link: '/dashboard/orders',
  },
];

export const sidebarNavs = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: dashboard,
  },
];

export const horizontalDefaultNavs = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: dashboard,
  },
];

export const minimalHorizontalMenus = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: dashboard,
  },
];
