import { Rowing } from '@material-ui/icons';
import {
  GET_CUSTOMERS,
  GET_CUSTOMER,
  UPDATE_CUSTOMER,
  CREATE_CUSTOMER,
  DELETE_CUSTOMER,
  CLEAR_CUSTOMER,
  GET_CUST_COMBOS,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  customers: [],
  customer: {},
  combos: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_CUSTOMERS: {
      return {
        ...state,
        customers: {
          data: action.payload.data,
          meta: action.payload.meta.pagination,
        },
      };
    }
    case GET_CUSTOMER: {
      return {
        ...state,
        customer: { ...action.payload, ...action.payload.attributes },
      };
    }
    case CREATE_CUSTOMER: {
      return {
        ...state,
        customer: { ...action.payload, ...action.payload.attributes },
      };
    }
    case UPDATE_CUSTOMER: {
      return {
        ...state,
        customer: { ...action.payload, ...action.payload.attributes },
      };
    }
    case DELETE_CUSTOMER: {
      return {
        ...state,
      };
    }
    case CLEAR_CUSTOMER: {
      return {
        ...state,
        customer: {},
      };
    }
    case GET_CUST_COMBOS: {
      return {
        ...state,
        combos: action.payload.data.map(row => {
          return {
            name: row.attributes.name,
            value: row.id,
          };
        }),
      };
    }
    default:
      return state;
  }
};
