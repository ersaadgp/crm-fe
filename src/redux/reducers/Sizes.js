import {
  GET_SIZES,
  GET_SIZE,
  UPDATE_SIZE,
  CREATE_SIZE,
  DELETE_SIZE,
  GET_SIZES_COMBO,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  sizes: [],
  size: {},
  sizesCombo: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_SIZES: {
      return {
        ...state,
        sizes: action.payload.sizes,
      };
    }
    case GET_SIZE: {
      return {
        ...state,
        size: action.payload,
      };
    }
    case CREATE_SIZE: {
      return {
        ...state,
        sizes: action.payload.sizes,
      };
    }
    case UPDATE_SIZE: {
      return {
        ...state,
        sizes: state.sizes.map(size =>
          size.id === action.payload.id ? action.payload : size,
        ),
      };
    }
    case GET_SIZES_COMBO: {
      return {
        ...state,
        sizesCombo: action.payload.sizes.map(row => {
          return { value: row.id, label: `Size ${row.size}` };
        }),
      };
    }
    case DELETE_SIZE: {
      return {
        ...state,
        sizes: state.sizes.filter(size => size.id !== action.payload),
      };
    }
    default:
      return state;
  }
};
