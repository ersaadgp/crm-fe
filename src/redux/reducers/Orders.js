import {
  GET_ORDERS,
  GET_ORDER,
  CREATE_ORDER,
  CREATE_ORDER_DETAIL,
  GET_ORDER_DETAIL,
  CLEAR_ORDER,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  orders: [],
  order: {},
  orderDetail: {},
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_ORDERS: {
      return {
        ...state,
        orders: {
          data: action.payload.data,
          meta: action.payload.meta.pagination,
        },
      };
    }
    case GET_ORDER: {
      return {
        ...state,
        order: { ...action.payload, ...action.payload.attributes },
      };
    }
    case CREATE_ORDER: {
      return {
        ...state,
        order: { ...action.payload, ...action.payload.attributes },
      };
    }
    case GET_ORDER_DETAIL: {
      return {
        ...state,
        orderDetail: { ...action.payload, ...action.payload.attributes },
      };
    }
    case CREATE_ORDER_DETAIL: {
      return {
        ...state,
        orderDetail: { ...action.payload, ...action.payload.attributes },
      };
    }
    case CLEAR_ORDER: {
      return {
        ...state,
        order: {},
      };
    }

    default:
      return state;
  }
};
