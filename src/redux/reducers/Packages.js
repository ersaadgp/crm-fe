import {
  GET_PACKAGES,
  GET_PACKAGE,
  UPDATE_PACKAGE,
  CREATE_PACKAGE,
  DELETE_PACKAGE,
  CLEAR_PACKAGE,
  GET_PACK_COMBOS,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  packages: [],
  _package: {},
  combos: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PACKAGES: {
      return {
        ...state,
        packages: {
          data: action.payload.data,
          meta: action.payload.meta.pagination,
        },
      };
    }
    case GET_PACKAGE: {
      return {
        ...state,
        _package: { ...action.payload, ...action.payload.attributes },
      };
    }
    case CREATE_PACKAGE: {
      return {
        ...state,
        _package: { ...action.payload, ...action.payload.attributes },
      };
    }
    case UPDATE_PACKAGE: {
      return {
        ...state,
        _package: { ...action.payload, ...action.payload.attributes },
      };
    }
    case DELETE_PACKAGE: {
      return {
        ...state,
      };
    }
    case CLEAR_PACKAGE: {
      return {
        ...state,
        _package: {},
      };
    }
    case GET_PACK_COMBOS: {
      return {
        ...state,
        combos: action.payload.data.map(row => {
          return {
            name: row.attributes.name,
            value: row.id,
          };
        }),
      };
    }
    default:
      return state;
  }
};
