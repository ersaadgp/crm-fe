import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, name } = param;
  const defaultUrl =
    URL +
    `/api/customers?pagination[start]=${page}&pagination[limit]=${pageSize}`;
  const searchUrl = `${defaultUrl}&filters[name][$contains]=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_CUSTOMERS, payload: res.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/customers/${id}`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const createData = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/customers`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const updateData = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/customers/${id}`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const deleteData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/customers/${id}`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(`Success delete data with id: ${id}`));
          dispatch({ type: type.DELETE_CUSTOMER, payload: id });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getCustCombo = param => {
  const { name } = param;
  const defaultUrl =
    URL + `/api/customers?pagination[start]=1&pagination[limit]=10`;
  const searchUrl = `${defaultUrl}&filters[name][$contains]=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_CUST_COMBOS, payload: res.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};
