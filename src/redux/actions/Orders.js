import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, name } = param;
  const defaultUrl =
    URL +
    `/api/orders?pagination[start]=${page}&pagination[limit]=${pageSize}&populate=*`;
  const searchUrl = `${defaultUrl}&filters[invoice_number][$contains]=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_ORDERS, payload: res.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/orders/${id}?populate=*`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_ORDER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const createData = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/orders`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({
            type: type.CREATE_ORDER,
            payload: { ...res.data.data, customer_id: payload.customer_id },
          });
          dispatch(
            createDataDetail({
              data: { ...payload.data, order: res.data.data.id },
            }),
          );
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getOneDetail = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/order-detail/${id}?populate=*`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_ORDER_DETAIL, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

const createDataDetail = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/order-details`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({
            type: type.CREATE_ORDER_DETAIL,
            payload: { ...res.data.data, customer_id: payload.customer_id },
          });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};
