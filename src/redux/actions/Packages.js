import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, name } = param;
  const defaultUrl =
    URL +
    `/api/travel-packages?pagination[start]=${page}&pagination[limit]=${pageSize}&populate=*`;
  const searchUrl = `${defaultUrl}&filters[name][$contains]=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PACKAGES, payload: res.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/travel-packages/${id}?populate=*`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PACKAGE, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const createData = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/travel-packages`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_PACKAGE, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const updateData = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/travel-packages/${id}`, payload)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_PACKAGE, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const deleteData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/travel-packages/${id}`)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(`Success delete data with id: ${id}`));
          dispatch({ type: type.DELETE_PACKAGE, payload: id });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};

export const getPackCombo = param => {
  const { name } = param;
  const defaultUrl =
    URL + `/api/travel-packages?pagination[start]=1&pagination[limit]=10`;
  const searchUrl = `${defaultUrl}&filters[name][$contains]=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PACK_COMBOS, payload: res.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError(error.message));
      });
  };
};
