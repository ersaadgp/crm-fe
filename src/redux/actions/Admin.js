import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import { DELETE_ADMIN, GET_ADMINS } from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getAdmins = param => {
  const { pageSize, page } = param;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/backoffice/users?pageSize=${pageSize}&page=${page}&role=2`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(data => {
        if (data.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: GET_ADMINS, payload: data.data.data });
        } else {
          dispatch(
            fetchError('There was something issue in responding server.'),
          );
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const deleteAdmin = (userId, callbackFun) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete('/users', { params: { id: userId } })
      .then(data => {
        if (data.status === 200) {
          dispatch(fetchSuccess('Selected user was deleted successfully.'));
          dispatch({ type: DELETE_ADMIN, payload: userId });
          if (callbackFun) callbackFun();
        } else {
          dispatch(
            fetchError('There was something issue in responding server.'),
          );
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};
