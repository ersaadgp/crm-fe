import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, name } = param;
  const defaultUrl = URL + `/api/products?pageSize=${pageSize}&page=${page}`;
  const searchUrl = `${defaultUrl}&name=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PRODUCTS, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/product/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PRODUCT, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const createData = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/product`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 201) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_PRODUCT, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const updateData = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/product/${id}`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_PRODUCT, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const deleteData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/product/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.DELETE_PRODUCT, payload: id });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getDataCombo = param => {
  const { page, name } = param;
  const defaultUrl = URL + `/api/products?pageSize=${5}&page=${page}`;
  const searchUrl = `${defaultUrl}&name=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PRODUCTS_COMBO, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};
