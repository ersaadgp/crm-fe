import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, dateParam } = param;
  const { date, start_date, end_date } = dateParam;
  const defaultUrl =
    URL + `/api/v2/transactions?pageSize=${pageSize}&page=${page}`;
  let searchUrl = '';

  if (date || start_date)
    searchUrl = date
      ? `${defaultUrl}&date=${date}`
      : `${defaultUrl}&start_date=${start_date}&end_date=${end_date}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!date && !start_date ? defaultUrl : searchUrl, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_TRANSACTIONS, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};
