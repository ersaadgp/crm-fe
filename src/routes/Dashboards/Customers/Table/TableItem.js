import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { alpha, makeStyles } from '@material-ui/core/styles';

import CmtAvatar from '../../../../@coremat/CmtAvatar';
import moment from 'moment';
import { useHistory } from 'react-router';

const def_img =
  'https://assets.adidas.com/images/w_600,f_auto,q_auto/4e894c2b76dd4c8e9013aafc016047af_9366/Superstar_Shoes_White_FV3284_01_standard.jpg';

const useStyles = makeStyles(theme => ({
  tableRowRoot: {
    cursor: 'pointer',
    position: 'relative',
    transition: 'all .2s',
    borderTop: `solid 1px ${theme.palette.borderColor.main}`,
    '&:hover': {
      backgroundColor: alpha(theme.palette.primary.main, 0.08),
      transform: 'translateY(-4px)',
      boxShadow: `0 3px 10px 0 ${alpha(theme.palette.common.dark, 0.2)}`,
      borderTopColor: 'transparent',
      '& $tableCellRoot': {
        color: theme.palette.text.primary,
        '&:last-child': {
          color: theme.palette.error.main,
        },
        '&.success': {
          color: theme.palette.success.main,
        },
      },
    },
    '&:last-child': {
      borderBottom: `solid 1px ${theme.palette.borderColor.main}`,
    },
  },
  tableCellRoot: {
    padding: 16,
    fontSize: 12,
    letterSpacing: 0.25,
    color: theme.palette.text.secondary,
    borderBottom: '0 none',
    position: 'relative',
    '&:first-child': {
      paddingLeft: 24,
    },
    '&:last-child': {
      color: theme.palette.error.main,
      paddingRight: 24,
    },
    '&.success': {
      color: theme.palette.success.main,
    },
    '& .Cmt-media-object': {
      alignItems: 'center',
    },
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
  productName: {
    fontSize: '0.8em',
  },
}));

const actions = [
  {
    label: 'Blokir',
  },
  {
    label: 'Aktifkan',
  },
];

function getBgColor(status) {
  const color = {
    cancelled: '#E00930',
    Aktif: '#0795F4',
    delayed: '#03DAC5',
    onHold: '#FF8C00',
  };

  switch (status) {
    case 1:
      return color['onHold'];
    case 0:
      return color['Aktif'];
  }
}

const TableItem = ({ row, id }) => {
  const classes = useStyles();
  const history = useHistory();

  const handleAction = () => {
    history.push(`/dashboard/customers/detail/${id}`);
  };

  const generateProduct = value => {
    const newValue = value.map(row => (
      <li className={classes.productName}>{row}</li>
    ));
    return newValue;
  };

  return (
    <TableRow className={classes.tableRowRoot} onClick={handleAction}>
      <TableCell className={classes.tableCellRoot}>{id}</TableCell>
      <TableCell className={classes.tableCellRoot}>{row?.name}</TableCell>
      <TableCell className={classes.tableCellRoot}>{row?.email}</TableCell>
      <TableCell className={classes.tableCellRoot}>{row?.phone}</TableCell>
      <TableCell className={classes.tableCellRoot}>{row?.address}</TableCell>

      <TableCell className={classes.tableCellRoot}></TableCell>
    </TableRow>
  );
};

export default TableItem;
