import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';

// component
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Grid, Box, Button } from '@material-ui/core';
import Modal from '../components/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { Edit, Save, Delete, ArrowBack } from '@material-ui/icons';

// state
import { useDispatch, useSelector } from 'react-redux';
import {
  getOneData,
  deleteData,
  createData,
  updateData,
} from 'redux/actions/Customers';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const DetailCustomers = () => {
  const { customer } = useSelector(({ customersReducer }) => customersReducer);

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { id, action } = useParams();

  const isUpdate = action === 'update';
  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState(0);
  const [isDisabled, setDisabled] = useState(true);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (id) dispatch(getOneData(id));
    else {
      setName('');
      setAddress('');
    }
  }, [id]);

  useEffect(() => {
    if (customer?.id && !isCreate) {
      setName(customer.name);
      setAddress(customer.address);
      setEmail(customer.email);
      setPhone(customer.phone);
    } else if (customer?.id && isCreate) {
      history.push(`/dashboard/customers/detail/${customer?.id}`);
    }
  }, [customer]);

  const data = [
    {
      label: 'Name',
      value: name,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Phone',
      value: phone,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Email',
      value: email,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Address',
      value: address,
      isDisabled: isUpdate || isCreate ? false : true,
    },
  ];

  const handleChange = (value, i) => {
    if (i === 1) setName(value);
    if (i === 2) setPhone(value);
    if (i === 3) setEmail(value);
    if (i === 4) setAddress(value);
  };

  const handleSubmit = () => {
    const payload = {
      data: {
        name: name,
        email: email,
        phone: phone,
        address: address,
      },
    };

    if (!customer?.id) {
      dispatch(createData(payload));
    } else {
      dispatch(updateData(customer?.id, payload));
      history.push(`/dashboard/customers/detail/${customer?.id}`);
    }
  };

  const handleDelete = () => {
    dispatch(deleteData(id));
    history.push('/dashboard/customers');
  };

  useEffect(() => {
    if (address && email && customer && phone) setDisabled(false);
  }, [address, email, customer]);

  return (
    <>
      <CmtCard className="mb-3">
        <CmtCardHeader
          className="pt-4"
          title="Customer Detail"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}></CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {customer && (
            <Grid container spacing={5}>
              {data.map((row, i) => (
                <Grid
                  item
                  lg={10}
                  xl={row.label === 'Address' ? 12 : 4}
                  key={i}>
                  <AppTextInput
                    variant="outlined"
                    label={row.label}
                    value={row.value}
                    onChange={e => handleChange(e.target.value, i + 1)}
                    disabled={row.isDisabled}
                    type={row.type || 'text'}
                  />
                </Grid>
              ))}
            </Grid>
          )}
        </CmtCardContent>
      </CmtCard>

      <Box display="flex" justifyContent="flex-end" className="mt-3">
        {isUpdate && (
          <Button
            color="secondary"
            variant="outlined"
            className="mr-3"
            onClick={() => history.push(`/dashboard/customers/detail/${id}`)}>
            Cancel
          </Button>
        )}
        {isDetail && (
          <>
            <Button
              color="default"
              variant="outlined"
              className="mr-3"
              onClick={() => history.push(`/dashboard/customers`)}>
              <ArrowBack className="mr-2" /> Back
            </Button>
            <Button
              className="mr-3"
              color="secondary"
              variant="outlined"
              onClick={() => setOpen(true)}>
              <Delete className="mr-2" /> Delete
            </Button>
            <Button
              color="primary"
              variant="contained"
              onClick={() => history.push(`/dashboard/customers/update/${id}`)}>
              <Edit className="mr-2" /> Edit
            </Button>
          </>
        )}
        {!isDetail && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
            disabled={isDisabled}>
            <Save className="mr-2" /> Save
          </Button>
        )}
      </Box>
      <Modal open={open} setOpen={setOpen} handleDelete={handleDelete} />
    </>
  );
};

export default DetailCustomers;
