import React from 'react';

import { Grid, Box } from '@material-ui/core';

import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Table from './Table';
import Detail from './Detail';
import Header from '../../../@jumbo/components/AppLayout/partials/custom/Header';
import { useParams } from 'react-router';

const CryptoDashboard = () => {
  const { action } = useParams();

  return (
    <>
      <Header />
      <PageContainer heading="Customers">
        {action ? (
          <Detail />
        ) : (
          <>
            {/* <Filter /> */}
            <Table />
          </>
        )}
      </PageContainer>
    </>
  );
};

export default CryptoDashboard;
