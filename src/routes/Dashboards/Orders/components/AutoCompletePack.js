import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from 'react-redux';
import { getPackCombo, getOneData } from 'redux/actions/Packages';

export default function Asynchronous({ isDetail, show, setShow }) {
  const dispatch = useDispatch();
  const { combos, _package } = useSelector(
    ({ packagesReducer }) => packagesReducer,
  );

  const { loading } = useSelector(({ common }) => common);
  const [value, setValue] = useState({ value: show });
  const [inputValue, setInputValue] = useState(show);

  useEffect(() => {
    dispatch(getPackCombo({ page: 1, name: inputValue }));
  }, [inputValue]);

  useEffect(() => {
    if (value?.value || show) {
      dispatch(getOneData(show));
      setShow(value?.value);
    }
  }, [value, show]);

  useEffect(() => {
    if (_package?.name && isDetail) {
      setInputValue(_package?.name);
    }
  }, [_package]);

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(e, newInputValue) => {
        setInputValue(newInputValue);
      }}
      getOptionLabel={combos => combos.name || ''}
      id="controllable-states-demo"
      options={combos}
      loading={loading}
      disabled={isDetail}
      renderInput={params => (
        <TextField
          {...params}
          label="Travel Package"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
