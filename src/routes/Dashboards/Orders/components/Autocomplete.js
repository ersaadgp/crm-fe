import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from 'react-redux';
import { getCustCombo, getOneData } from 'redux/actions/Customers';

export default function Asynchronous({ isDetail, show, setCustomer }) {
  const dispatch = useDispatch();
  const { combos, customer } = useSelector(
    ({ customersReducer }) => customersReducer,
  );
  const { loading } = useSelector(({ common }) => common);
  const [value, setValue] = useState({ value: show });
  const [inputValue, setInputValue] = useState(show);

  useEffect(() => {
    dispatch(getCustCombo({ page: 1, name: inputValue }));
  }, [inputValue]);

  useEffect(() => {
    if (value?.value || show) {
      dispatch(getOneData(show));
      setCustomer(value?.value);
    }
  }, [value, show]);

  useEffect(() => {
    if (customer?.name && isDetail) {
      setInputValue(customer?.name);
    }
  }, [customer]);

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(e, newInputValue) => {
        setInputValue(newInputValue);
      }}
      getOptionLabel={combos => combos.name || ''}
      id="controllable-states-demo"
      options={combos}
      loading={loading}
      disabled={isDetail}
      renderInput={params => (
        <TextField
          {...params}
          label="Customer"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
