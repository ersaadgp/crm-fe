import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';

// component
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Grid, Box, Button } from '@material-ui/core';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { Save, ArrowBack } from '@material-ui/icons';
import AutoComplete from '../components/Autocomplete';
import AutoCompletePack from '../components/AutoCompletePack';

// state
import { useDispatch, useSelector } from 'react-redux';
import { getOneData, createData } from 'redux/actions/Orders';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const DetailOrders = () => {
  const { order, orderDetail } = useSelector(
    ({ ordersReducer }) => ordersReducer,
  );

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { id, action } = useParams();

  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const [invoice, setInvoice] = useState('');
  const [customer, setCustomer] = useState('');
  const [totalPrice, setTotalPrice] = useState('');
  const [_package, setPackage] = useState('');

  const [isDisabled, setDisabled] = useState(true);

  useEffect(() => {
    if (id) dispatch(getOneData(id ?? order?.id));
  }, [id]);

  useEffect(() => {
    if (order?.id && !isCreate) {
      setInvoice(order.invoice_number);
      setCustomer(order?.customer_id?.data?.id ?? order?.customer_id);
      setTotalPrice(order.total_price);
    } else if (isCreate && order?.id) {
      history.push(`/dashboard/orders/detail/${order?.id}`);
    }
  }, [order]);

  const handleSubmit = () => {
    const payload = {
      data: {
        invoice_number: invoice,
        total_price: parseInt(totalPrice),
        customer_id: customer,
        price: parseInt(totalPrice),
        package: _package,
      },
    };

    console.log(payload);

    if (!order?.id) {
      dispatch(createData(payload));
    }
  };

  useEffect(() => {
    if (customer && totalPrice && invoice) setDisabled(false);
  }, [customer, invoice, totalPrice]);

  console.log(orderDetail);

  return (
    <>
      <CmtCard className="mb-3">
        <CmtCardHeader
          className="pt-4"
          title="Order Detail"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}></CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {order && (
            <Grid container spacing={5}>
              <Grid item lg={10} xl={7}>
                <AppTextInput
                  variant="outlined"
                  label="Invoice"
                  value={invoice}
                  onChange={e => setInvoice(e.target.value)}
                  disabled={isDetail}
                />
              </Grid>
              <Grid item lg={10} xl={7}>
                <AutoComplete
                  isDetail={isDetail}
                  show={customer}
                  setCustomer={setCustomer}
                />
              </Grid>
              <Grid item lg={10} xl={7}>
                <AppTextInput
                  variant="outlined"
                  label="Total Price"
                  value={totalPrice}
                  onChange={e => setTotalPrice(e.target.value)}
                  disabled={isDetail}
                  type="number"
                />
              </Grid>
              <Grid item lg={10} xl={7}>
                <AutoCompletePack
                  isDetail={isDetail}
                  show={_package}
                  setShow={setPackage}
                />
              </Grid>
            </Grid>
          )}
        </CmtCardContent>
      </CmtCard>

      <Box display="flex" justifyContent="flex-end" className="mt-3">
        {isDetail && (
          <>
            <Button
              color="default"
              variant="outlined"
              className="mr-3"
              onClick={() => history.push(`/dashboard/orders`)}>
              <ArrowBack className="mr-2" /> Back
            </Button>
          </>
        )}
        {isCreate && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
            disabled={isDisabled}>
            <Save className="mr-2" /> Save
          </Button>
        )}
      </Box>
    </>
  );
};

export default DetailOrders;
