import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '../../@jumbo/components/PageComponents/PageLoader';

const Dashboards = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Redirect
          exact
          from={`${requestedUrl}/`}
          to={`${requestedUrl}/travel-packages`}
        />

        {/* Travel Packages */}
        <Route
          exact
          path={`${requestedUrl}/travel-packages`}
          component={lazy(() => import('./Packages'))}
        />
        <Route
          exact
          path={`${requestedUrl}/travel-packages/:action`}
          component={lazy(() => import('./Packages'))}
        />
        <Route
          exact
          path={`${requestedUrl}/travel-packages/:action/:id`}
          component={lazy(() => import('./Packages'))}
        />

        {/* Customers */}
        <Route
          exact
          path={`${requestedUrl}/customers`}
          component={lazy(() => import('./Customers'))}
        />
        <Route
          exact
          path={`${requestedUrl}/customers/:action`}
          component={lazy(() => import('./Customers'))}
        />
        <Route
          exact
          path={`${requestedUrl}/customers/:action/:id`}
          component={lazy(() => import('./Customers'))}
        />

        {/* Orders */}
        <Route
          exact
          path={`${requestedUrl}/orders`}
          component={lazy(() => import('./Orders'))}
        />
        <Route
          exact
          path={`${requestedUrl}/orders/:action`}
          component={lazy(() => import('./Orders'))}
        />
        <Route
          exact
          path={`${requestedUrl}/orders/:action/:id`}
          component={lazy(() => import('./Orders'))}
        />

        {/* Records */}
        <Route
          exact
          path={`${requestedUrl}/records`}
          component={lazy(() => import('./Records'))}
        />
        <Route
          exact
          path={`${requestedUrl}/products/:action`}
          component={lazy(() => import('./Products'))}
        />
        <Route
          exact
          path={`${requestedUrl}/records/:action/:id`}
          component={lazy(() => import('./Records'))}
        />

        {/* <Route component={lazy(() => import('../ExtraPages/404'))} /> */}
      </Switch>
    </Suspense>
  );
};

export default Dashboards;
