import React from 'react';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Table from './Table';
import Detail from './Detail';
import Header from '../../../@jumbo/components/AppLayout/partials/custom/Header';
import { useParams } from 'react-router';

const CryptoDashboard = () => {
  const { action } = useParams();

  return (
    <>
      <Header />
      <PageContainer heading="Travel Packages">
        {action ? (
          <Detail />
        ) : (
          <>
            {/* <Filter /> */}
            <Table />
          </>
        )}
      </PageContainer>
    </>
  );
};

export default CryptoDashboard;
