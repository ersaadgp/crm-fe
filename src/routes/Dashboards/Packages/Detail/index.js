import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import { getBase64 } from '../components/helper';
import { useDropzone } from 'react-dropzone';

// component
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Grid, Box, Button } from '@material-ui/core';
import Modal from '../components/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { Edit, Save, Delete, ArrowBack } from '@material-ui/icons';

// state
import { useDispatch, useSelector } from 'react-redux';
import {
  getOneData,
  deleteData,
  createData,
  updateData,
} from 'redux/actions/Packages';

const url = process.env.REACT_APP_BASE_URL;
const def_img =
  'https://pertaniansehat.com/v01/wp-content/uploads/2015/08/default-placeholder.png';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const DetailPackages = () => {
  const { _package } = useSelector(({ packagesReducer }) => packagesReducer);

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { id, action } = useParams();

  const isUpdate = action === 'update';
  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState('');
  const [isDisabled, setDisabled] = useState(true);
  const [open, setOpen] = useState(false);

  const [picture, setPicture] = useState('');
  const [resPic, setResPic] = useState(def_img);

  useEffect(() => {
    if (id) dispatch(getOneData(id));
  }, [id]);

  useEffect(() => {
    if (_package?.id && !isCreate) {
      setName(_package.name);
      setPrice(_package.price);
      setDescription(_package.description);
      setPicture(url + _package?.image?.data?.attributes?.url);
    } else if (_package?.id && isCreate) {
      history.push(`/dashboard/travel-packages/detail/${_package?.id}`);
    }
  }, [_package]);

  const data = [
    {
      label: 'Name',
      value: name,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Price',
      value: price,
      isDisabled: isUpdate || isCreate ? false : true,
      type: 'number',
    },
    {
      label: 'Description',
      value: description,
      isDisabled: isUpdate || isCreate ? false : true,
    },
  ];

  const handleChange = (value, i) => {
    if (i === 0) setName(value);
    if (i === 1) setPrice(value);
    if (i === 2) setDescription(value);
  };

  const handleSubmit = async () => {
    let imgChecker = '';
    if (picture) imgChecker = picture.substring(0, 4);
    const payload = {
      data: {
        name,
        description,
        price: parseInt(price),
      },
    };
    const formData = new FormData();
    formData.append('data', payload);
    formData.append('files', resPic[0]);

    // if (imgChecker !== 'http') payload.data.image = picture;
    if (!_package?.id) {
      dispatch(createData(payload));
    } else {
      dispatch(updateData(_package?.id, payload));
      history.push(`/dashboard/travel-packages/detail/${_package?.id}`);
    }
  };

  const handleDelete = () => {
    dispatch(deleteData(id));
    history.push('/dashboard/travel-packages');
  };

  useEffect(() => {
    if (name && price && picture) setDisabled(false);
  }, [name, price, picture]);

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setResPic(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });

  useEffect(() => {
    if (resPic.length > 0) {
      const file = resPic[0];
      getBase64(file)
        .then(result => {
          file['base64'] = result;
          setPicture(result);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }, [resPic]);

  return (
    <>
      <CmtCard className="mb-3">
        <CmtCardHeader
          className="pt-4"
          title="Travel Package Detail"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}></CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {_package && (
            <Box display="flex">
              <Box
                display="flex"
                justifyContent="center"
                {...getRootProps()}
                className={`${!isDetail && 'pointer'} mb-5`}>
                <input {...getInputProps()} disabled={isDetail} />
                <img
                  height={240}
                  width={240}
                  style={{ maxHeight: '300px' }}
                  src={picture ? picture : resPic}
                  onError={() => {
                    setPicture(def_img);
                  }}
                />
              </Box>
              <Grid container spacing={5} className="ml-3">
                {data.map((row, i) => (
                  <Grid item lg={9} xl={5} key={i}>
                    <AppTextInput
                      label={row.label}
                      value={row.value}
                      onChange={e => handleChange(e.target.value, i)}
                      disabled={row.isDisabled}
                      type={row.type || 'text'}
                    />
                  </Grid>
                ))}
              </Grid>
            </Box>
          )}
        </CmtCardContent>
      </CmtCard>

      <Box display="flex" justifyContent="flex-end" className="mt-3">
        {isUpdate && (
          <Button
            color="secondary"
            variant="outlined"
            className="mr-3"
            onClick={() =>
              history.push(`/dashboard/travel-packages/detail/${id}`)
            }>
            Cancel
          </Button>
        )}
        {isDetail && (
          <>
            <Button
              color="default"
              variant="outlined"
              className="mr-3"
              onClick={() => history.push(`/dashboard/travel-packages`)}>
              <ArrowBack className="mr-2" /> Back
            </Button>
            <Button
              className="mr-3"
              color="secondary"
              variant="outlined"
              onClick={() => setOpen(true)}>
              <Delete className="mr-2" /> Delete
            </Button>
            <Button
              color="primary"
              variant="contained"
              onClick={() =>
                history.push(`/dashboard/travel-packages/update/${id}`)
              }>
              <Edit className="mr-2" /> Edit
            </Button>
          </>
        )}
        {!isDetail && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
            disabled={isDisabled}>
            <Save className="mr-2" /> Save
          </Button>
        )}
      </Box>
      <Modal open={open} setOpen={setOpen} handleDelete={handleDelete} />
    </>
  );
};

export default DetailPackages;
