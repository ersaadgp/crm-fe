import * as React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { TextField, Grid } from '@material-ui/core';
import AppTextInput from '../../../../@jumbo/components/Common/formElements/AppTextInput';
import AppSelectBox from '@jumbo/components/Common/formElements/AppSelectBox';

export default function SimpleAccordion() {
  return (
    <div className="mb-3">
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header">
          <Typography className="ml-3">Filter</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container className="ml-3 mr-3 mb-5" spacing={3}>
            <Grid item md={4}>
              <AppTextInput
                fullWidth
                variant="outlined"
                label="Nama"
                // value={kategori}
                // onChange={e => onCategoryChange(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <AppTextInput
                fullWidth
                variant="outlined"
                label="Bidang"
                // value={kategori}
                // onChange={e => onCategoryChange(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <AppTextInput
                fullWidth
                variant="outlined"
                label="No. Kontak"
                // value={kategori}
                // onChange={e => onCategoryChange(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <AppTextInput
                fullWidth
                variant="outlined"
                label="Email"
                // value={kategori}
                // onChange={e => onCategoryChange(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <AppSelectBox
                fullWidth
                variant="outlined"
                label="Status"
                data={[{ title: 'Label', slug: 1 }]}
                valueKey="slug"
                labelKey="title"
                // value={kategori}
                // onChange={e => onCategoryChange(e.target.value)}
              />
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
