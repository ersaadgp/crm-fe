import React, { useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import { Box, Chip, List, ListItem, Typography } from '@material-ui/core';

const BasicExample = ({ setAttachment, attachment }) => {
  const thumbs = attachment.map((file, i) => (
    <Chip
      style={{ margin: '5px 2px', maxWidth: '150px' }}
      key={i}
      label={`${file.path} - ${file.size / 1000} KB`}
      color="primary"
      onDelete={() => handleDelete(file)}
      className="primary"
      variant="outlined"
    />
  ));

  const handleDelete = value => {
    const newValue = attachment.filter(row => row !== value);
    setAttachment(newValue);
  };

  const { getRootProps, getInputProps } = useDropzone({
    multiple: true,
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setAttachment(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      attachment.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [attachment],
  );

  return (
    <Box>
      <Box {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <Typography>
          Drag 'n' drop some files here, or click to select files
        </Typography>
      </Box>
      <aside>
        <List>{thumbs}</List>
      </aside>
    </Box>
  );
};

export default BasicExample;
