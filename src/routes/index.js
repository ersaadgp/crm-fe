import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import Dashboards from './Dashboards';
import Login from './Auth/Login';
import Signup from './Auth/Register';
import ForgotPassword from './Auth/ForgotPassword';
import ChangePassword from './Auth/ChangePassword';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

// import LayoutBuilder from './LayoutBuilder';

const Routes = () => {
  const { authUser } = useSelector(({ auth }) => auth);
  const location = useLocation();
  const token = !!localStorage.getItem('token');

  // if (
  //   location.pathname === '' ||
  //   location.pathname === '/' ||
  //   (!['/signin', '/signup', '/forgot-password'].includes(location.pathname) &&
  //     !token)
  // ) {
  //   return <Redirect to={'/signin'} />;
  // } else if (token && location.pathname === '/signin') {
  //   return <Redirect to={'/dashboard/products'} />;
  // }

  return (
    <React.Fragment>
      <Switch>
        <Route path="/dashboard" component={Dashboards} />
        <Route path="/signin" component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/forgot-password" component={ForgotPassword} />
        <Route path="/change-password" component={ChangePassword} />
        {/*<Route path="/layout-builder" component={LayoutBuilder} />*/}
      </Switch>
    </React.Fragment>
  );
};

export default Routes;
