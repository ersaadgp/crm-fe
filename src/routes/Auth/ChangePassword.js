import React from 'react';
import ChangePassword from '../../@jumbo/components/Common/authComponents/ChangePassword';

const ChangePasswordPage = () => (
  <ChangePassword variant="standard" wrapperVariant="bgColor" />
);

export default ChangePasswordPage;
